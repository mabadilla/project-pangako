var keystone = require('keystone'),
	Types = keystone.Field.Types;

/**
 * People Model
 * ==========
 */

var People = new keystone.List('People', {
    track: true
});

People.add({
    name: { type: Types.Name, required: true, index: true },
    isFeatured: { type: Boolean, label: 'Show name on homepage' }
}, 'Pulse', {
    antis: { type: Types.Relationship, ref: 'Topic', many: true },
    pros: { type: Types.Relationship, ref: 'Topic', many: true }
});

/**
 * Registration
 */

People.defaultColumns = 'name, isFeatured';
People.register();
