/**
 * Created by mjmaix on 9/2/14.
 */
var keystone = require('keystone'),
    Types = keystone.Field.Types;

/**
 *  Topic Model
 *  ===========
 */

var Topic  =  new keystone.List('Topic', {
    map: { name: 'title' },
    autokey: { path: 'slug', from: 'title', unique: true }
});

Topic.add({
    title: { type: String, required: true },
    state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
    author: { type: Types.Relationship, ref: 'User', index: true },
    categories: { type: Types.Relationship, ref: 'Category', many: true }
}, 'Config', {
    isFeatured: { type: Boolean, label: 'Show name on homepage', index: true }
});

Topic.relationship({ ref: 'People', path: 'people', refPath: 'pros' });
Topic.relationship({ ref: 'People', path: 'people', refPath: 'antis' });

Topic.register();