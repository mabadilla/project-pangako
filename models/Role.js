var keystone = require('keystone'),
	Types = keystone.Field.Types;

/**
 * Role Model
 * ==================
 */

var Role = new keystone.List('Role', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Role.add({
	name: { type: String, required: true }
});

Role.relationship({ ref: 'User', path: 'roles', refPath: 'users' });

Role.register();
