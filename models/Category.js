var keystone = require('keystone'),
	Types = keystone.Field.Types;

/**
 * Category Model
 * ==================
 */

var Category = new keystone.List('Category', {
	autokey: { from: 'name', path: 'key', unique: true }
});

Category.add({
	name: { type: String, required: true }
});

Category.relationship({ ref: 'Post', path: 'categories', refPath: 'posts' });
Category.relationship({ ref: 'Topic', path: 'categories', refPath: 'topics' });

Category.register();
