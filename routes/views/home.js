var keystone = require('keystone'),
    _ = require('underscore');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;
	
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';
    locals.hideSignIn = true;
    locals.data = {
            topics : [],
            people: []
        };
    

    // Load featured people
    view.on('init', function(next) {

        var q = keystone.list('People').model.find().where('isFeatured', true).populate('antis pros');

        q.exec(function(err, results) {
            if (err) return res.err(err);
            if (!results) return res.notfound('No featured people available');
            locals.people = results;
            next();
        });

    });
    
    // Load featured topics
    view.on('init', function(next) {

        var q = keystone.list('Topic').model.find().where('isFeatured', true).limit('4');

        q.exec(function(err, results) {
            if (err) return res.err(err);
            if (!results) return res.notfound('No featured topics available');
            locals.topics = results;
            next();
        });

    });

    // sample data
    locals.tableData = {
        headers : [ "Names", "FOI", "Death", "Gay Marriage"],
        rows: [
            {
                name: "Miriam",
                foi: true,
                death: false,
                gay: true
            },
            {
                name: "Roxas",
                foi: true,
                death: true,
                gay: false
            },
            {
                name: "Binay",
                foi: false,
                death: true,
                gay: false
            },
            {
                name: "Cayetano",
                foid: true,
                death: true,
                gay: true
            }
        ]
    };

	// Render the view
	view.render('home', function(){
        console.dir(locals.topics);
    });
	
};
